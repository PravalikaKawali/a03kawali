var path = require("path");
var express = require("express");
var logger = require("morgan");
var bodyParser = require("body-parser");
var app = express(); 
var server = require('http').createServer(app); 
app.set("views", path.resolve(__dirname, "assets")); 
app.set("view engine", "ejs");
app.use(express.static(__dirname + '/assets'));
var entries = [];
app.locals.entries = entries;
app.use(logger("dev")); 

app.use(bodyParser.urlencoded({ extended: false }));

app.get("/", function (request, response) {
    response.sendFile(__dirname+"/assets/indexes.html");
});
app.get("/home", function (request, response) {
    response.sendFile(__dirname+"/assets/indexes.html");
});
app.get("/yourchoice", function (request, response) {
    response.sendFile(__dirname+"/assets/YourChoice.html");
});
app.get("/contact", function (request, response) {
    response.sendFile(__dirname+"/assets/Contact.html");
});
app.get("/new-entry", function (request, response) {
    response.render("new-entry");
});
app.get("/guestbook", function (request, response) {
    response.render("index");
});
// POSTS
app.post("/new-entry", function (request, response) {
    if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.");
    return;
    }
    entries.push({ 
    title: request.body.title,
    content: request.body.body,
    published: new Date()
    });
    response.redirect("/guestbook");
    });
app.post("/contact", function (request, response) {
    var api_key = 'key-a6e22d1d1a420c73fc2dcfb419e02c69';
    var domain = 'sandbox171e471266b34315b10fc87cb8cbb3a4.mailgun.org';
    var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
     
    var data = {
      from: 'Pravalika <postmaster@sandbox171e471266b34315b10fc87cb8cbb3a4.mailgun.org>',
      to: 'pravalikakawali@gmail.com',
      subject: request.body.firstname,
      text: request.body.subject
    };
     
    mailgun.messages().send(data, function (error, body) {
      console.log(body);
      if(!error)
        response.send("true"+error);
      else
        response.send("false"+error);
    });
});
// 404
app.use(function (request, response) {
    response.status(404).render("404");
    });
// Listen for an application request on port 8081
server.listen(8081, function () {
 console.log('Guestbook app listening on http://127.0.0.1:8081/');
});